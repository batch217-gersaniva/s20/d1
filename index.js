console.log("Hello World");

//[SECTION] While Loop
//"Iteration" term for the repetition of statements


// SYNTAX -> while (expression/condition){statement}

let count = 5;

while(count !== 0){
	console.log("While: " + count);
	count--;   // (--) --> decrementation, decreasing the value by 1.
}

//[SECTION] Do-While Loop
// do{statement}
// while(expression/condition)

let number = Number(prompt("Give me a number"));

do{
	console.log("Do While: " + number);

// Increases


	number += 1;
	// number = number +1;
} while(number <= 10);


// [SECTION] For Loops

// SYNTAX
	 
	 // for (initialization; expression/condition; finalization) {statement
	 // }


// (++) --> incrementation, adding 1 to a variable value.

// for (let count = 0; count <= 20; count++){
// 	console.log(count);
// }

// for (let count = 0; count <= 14; count++){
// 	console.log(count);
// }

let myString = "ALex";
console.log(myString.length);

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);

// console.log(myString[-1]);

for(let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}

// Create a string name "myName"

let myName = "Abraham";

for (let i = 0; i < myName.length; i++){
	// console.log(myName[i].toLowerCase());

	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 
		){
		console.log(3)
	}else{
	console.log(myName[i]);
	}
}


//[SECTION] Continue and Break statements 

for (let count = 0; count <= 20; count++){

	if (count % 2 === 0){
		continue;
	}
	console.log("Continue and break: " + count);

	if (count > 10) {
		break;
	}
}

let name = "alexandro"

for (let i = 0; i < name.length; i++){
	console.log(name[i]);

	if(name[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration");
		continue;
	}

	if(name[i] == "d"){
		break;
	}
}